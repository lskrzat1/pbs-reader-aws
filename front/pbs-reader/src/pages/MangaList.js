import React, { useEffect, useState } from 'react';
import { getToken, getUser, removeUserSession } from '../utils/common';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import './css/MangaList.css';  // Upewnij się, że masz odpowiednie style w tym pliku

const MangaList = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useNavigate();
  const user = getUser();
  const token = getToken();

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch('https://vj2zf77xja.execute-api.eu-north-1.amazonaws.com/pbs/mangalist');

      if (response.ok) {
        const jsonData = await response.json();
        setData(jsonData);
        setLoading(false);
      } else {
        console.error('Failed to fetch data');
      }
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div className="manga-list-container">
      <h1>Dane z API</h1>
      <div className="manga-grid">
        {data.map((item, index) => (
          <div key={index} className="manga-tile">
            <img src={item.presigned_url} alt={item.name} className="manga-cover" />
            <h3 className="manga-name">{item.name}</h3>
            <p className="manga-description">{item.description}</p>
            <Link to={`/ChapterList/${item.id}`} className="manga-button">Wyświetl</Link>
          </div>
        ))}
      </div>
      <div className="add-button-container">
        <Link className="add-button" to="/NewManga">
          Dodaj nową mangę
        </Link>
      </div>
    </div>
  );
};

export default MangaList;
