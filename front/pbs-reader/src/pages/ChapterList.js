import React, { useEffect, useState } from 'react';
import { getToken } from '../utils/common';
import { useParams, Link } from 'react-router-dom';
import './css/ChapterList.css'; // importuj nowy plik CSS

const ChapterList = () => {
  const [chapters, setChapters] = useState(null);
  const [productName, setProductName] = useState(null);
  const [loading, setLoading] = useState(true);
  const { id } = useParams();
  const token = getToken();

  useEffect(() => {
    fetchData();
    fetchProductName();
  }, []);

  useEffect(() => {
    fetchData();
    fetchProductName();
  }, []);

  const fetchData = async () => {
    try {
      const response = await fetch(`https://vj2zf77xja.execute-api.eu-north-1.amazonaws.com/pbs/chapterlist/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });

      if (response.ok) {
        const jsonData = await response.json();
        setChapters(jsonData);
        setLoading(false);
      } else if (response.status === 404) {
        setChapters([]); // Jeśli serwer zwróci 404, ustaw chapters na pustą tablicę
        setLoading(false);
      } else {
        console.error('Failed to fetch data');
      }
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  const fetchProductName = async () => {
    try {
      const response = await fetch(`/api/product/${id}`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });

      if (response.ok) {
        const jsonData = await response.json();
        setProductName(jsonData.name);
      } else {
        console.error('Failed to fetch product name');
      }
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
    }
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>Lista rozdziałów dla mangi {productName}</h1>
      <div className="chapter-list">
        {chapters?.map((chapter, index) => (
          <div className="chapter-card" key={index}>
            <h3>{chapter.name}</h3>
            <Link to={`/chapter/${chapter.id}`}>
              Wyświetl
            </Link>
          </div>
        ))}
      </div>
      <div className="add-button-container">
        <Link className="add-button" to={`/ChapterList/${id}/add`}>
          Dodaj nowy rozdział
        </Link>
      </div>
    </div>
  );
};

export default ChapterList;
