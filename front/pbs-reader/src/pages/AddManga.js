import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './css/AddMangaButton.css';

const NewManga = () => {
  const [inputName, setInputName] = useState('');
  const [inputDescription, setInputDescription] = useState('');
  const [file, setFile] = useState(null);
  const [status, setStatus] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isUploading, setIsUploading] = useState(false);
  const [uploadProgress, setUploadProgress] = useState(0);
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    setIsLoading(true);
    const name = inputName;
    const description = inputDescription;

    try {
      const response = await fetch('https://vj2zf77xja.execute-api.eu-north-1.amazonaws.com/pbs/mangalist/addmanga', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name, description, filename: file ? file.name : '' }),
      });

      setIsLoading(false);  // Reset the loading flag here

      if (response.ok) {
        const data = await response.json();
        const presignedUrl = data.presigned_url;

        if (presignedUrl && file) {
          setIsUploading(true);  // Set uploading flag here

          const xhr = new XMLHttpRequest();
          xhr.open("PUT", presignedUrl, true);

          xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
              const percentComplete = (e.loaded / e.total) * 100;
              setUploadProgress(Math.round(percentComplete));
            }
          };

          xhr.onload = function() {
            if (xhr.status === 200) {
              setStatus('Element i plik dodane pomyślnie.');
              navigate(-1);
            }
            setIsUploading(false);  // Reset the uploading flag here
          };

          xhr.send(file);
        } else {
          setStatus('Element dodany pomyślnie, ale brak pliku lub presigned URL.');
        }
      } else {
        setStatus('Błąd przy dodawaniu elementu.');
      }
    } catch (error) {
      setStatus(`Wystąpił błąd: ${error.toString()}`);
    }
  };

  const isFormValid = inputName && inputDescription && file;

  return (
    <div className="form-container">
      <h1>Dodaj element</h1>
      <form onSubmit={handleSubmit}>
        <div className="input-group">
          <label>Nazwa</label>
          <input
            type="text"
            value={inputName}
            onChange={(e) => setInputName(e.target.value)}
            placeholder="Nazwa"
            required
          />
        </div>

        <div className="input-group">
          <label>Opis</label>
          <textarea
            value={inputDescription}
            onChange={(e) => setInputDescription(e.target.value)}
            placeholder="Opis"
            required
          ></textarea>
        </div>

        <div className="input-group">
          <label>Okładka</label>
          <input
            type="file"
            onChange={(e) => setFile(e.target.files[0])}
            required
          />
        </div>

        <div style={{ marginTop: '20px' }}>
          <button type="submit" className="add-button" disabled={!isFormValid}>Dodaj</button>
        </div>
      </form>
      {status && <div>Status: {status}</div>}

      {(isLoading || isUploading) && (
        <div className="overlay">
          <div className="progress-bar">
            <div className="progress" style={{width: `${uploadProgress}%`}}></div>
          </div>
          <p>Uploading: {uploadProgress}%</p>
        </div>
      )}
    </div>
  );
};

export default NewManga;