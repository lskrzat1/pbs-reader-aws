import React, { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import axios from 'axios';
import './css/AddChapterButton.css';
import './css/LoadingOverlayChapter.css';

async function getPresignedUrl(fileName, chapterId, productId) {
  try {
    const response = await axios.post('https://vj2zf77xja.execute-api.eu-north-1.amazonaws.com/pbs/tst', {
      fileName,
      chapterId,
      productId
    });
    return response.data.url;
  } catch (error) {
    console.error('Error getting presigned URL:', error);
    return null;
  }
}

const AddChapter = () => {
  const [name, setName] = useState('');
  const [files, setFiles] = useState([]);
  const [productName, setProductName] = useState(null);
  const [date, setDate] = useState('');
  const { id } = useParams();
  const navigate = useNavigate();
  const [chapterId, setChapterId] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [progress, setProgress] = useState(0);

  const totalFileSize = files.reduce((acc, file) => acc + file.size, 0);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    setProgress(0);

    const detailsSuccess = await sendChapterDetails();

    if (detailsSuccess) {
      const filesSuccess = await sendChapterFiles(detailsSuccess);
      if (filesSuccess) {
        navigate(`/ChapterList/${id}`);
      }
    }

    setIsLoading(false);
  };

  const sendChapterDetails = async () => {
    try {
      const response = await axios.post('https://vj2zf77xja.execute-api.eu-north-1.amazonaws.com/pbs/chapterlist/addchapter', {
        name,
        productId: id,
        date
      });
      setChapterId(response.data.id);
      return response.data.id;
    } catch (error) {
      console.error('There was a problem with the axios operation:', error);
      return false;
    }
  };

  const sendChapterFiles = async (chapterId) => {
    try {
      for (const file of files) {
        const url = await getPresignedUrl(file.name, chapterId, id);
        if (!url) return false;

        await fetch(url, {
          method: 'PUT',
          body: file,
          headers: {
            'Content-Type': file.type
          },
        })
        .then(response => {
          setProgress(oldProgress => oldProgress + file.size);
          return response;
        });
      }
      return true;
    } catch (error) {
      console.error('There was a problem with the fetch operation:', error);
      return false;
    }
  };

  return (
    <div className="add-chapter-form">
      {isLoading && (
        <div className="loading-overlay">
          <div className="loader"></div>
          <div className="progress-bar" style={{width: `${(progress / totalFileSize) * 100}%`}}></div>
        </div>
      )}
      <h1>Add a new chapter for Manga: {productName}</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Chapter Name:
          <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
        </label>
        <label>
          Upload Files:
          <input type="file" multiple onChange={(e) => setFiles(Array.from(e.target.files))} />
        </label>
        <label>
          Date of Creation (DD-MM-YYYY):
          <input type="text" value={date} onChange={(e) => setDate(e.target.value)} />
        </label>
        <button type="submit" className="add-button">Add Chapter</button>
      </form>
    </div>
  );
};

export default AddChapter;
