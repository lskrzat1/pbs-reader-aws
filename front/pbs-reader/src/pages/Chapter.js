import React, { useState, useEffect } from 'react';
import { useParams, Link , useNavigate} from 'react-router-dom';
import './css/Chapter.css'; // importuj nowy plik CSS

function Chapter() {
  const navigate = useNavigate();
  const { chapterId } = useParams();
  const [fileDataList, setFileDataList] = useState([]);
  
  useEffect(() => {
    const fetchImage = async () => {
      try {
        const response = await fetch(`https://vj2zf77xja.execute-api.eu-north-1.amazonaws.com/pbs/view?chapterId=${chapterId}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
          },
        });

        if (response.ok) {
          const { data } = await response.json();
          console.log("Received data:", data);
          setFileDataList(data || []);
        } else {
          console.error('Server response was not ok.');
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchImage();
  }, [chapterId]);

  return (
    <div>
      <div className="back-button-container">
        <button onClick={() => navigate(-1)} className="back-button">Powrót</button>
      </div>
      <div className="image-container">
        {fileDataList.map((fileData, index) => (
          <div className="image-box" key={index}>
            <img 
              src={fileData.s3_presigned_url} 
              alt={fileData.file_name} 
              className="image" 
            />
          </div>
        ))}
      </div>
    </div>
  );
}

export default Chapter;
