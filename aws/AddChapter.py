import json
import psycopg2
import uuid

def lambda_handler(event, context):
    # Generuj unikatowe ID
    unique_id = str(uuid.uuid4())
    
    # Odczytaj 'name' z ciała zapytania POST
    body = json.loads(event.get('body', '{}'))
    name = body.get('name', '')
    product_id = body.get('productId', '')
    
    # Połączenie z bazą danych
    conn = psycopg2.connect(
            user="postgres",
            password="123Askone",
            host="pbs-reader-2.cvn9enzqebxx.eu-north-1.rds.amazonaws.com",
            port="5432",
            database="pbs_reader"
    )

    # Inicjalizuj wynik
    result = {}
    
    try:
        # Wykonaj zapytanie SQL
        with conn.cursor() as cur:
            cur.execute("INSERT INTO productmanager.chapters (id, name, product_id) VALUES (%s, %s, %s);", (unique_id, name, product_id))
        conn.commit()
        
        result = {'status': 'success', 'id': unique_id}  # Dodaj unique_id do wyniku
    except Exception as e:
        result = {'status': 'error', 'error': str(e)}
    finally:
        # Zamknij połączenie
        conn.close()
        
    return {
        'statusCode': 200,
        'body': json.dumps(result),
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        }
    }