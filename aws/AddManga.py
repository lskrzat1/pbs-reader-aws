import json
import psycopg2
import uuid
import boto3

def lambda_handler(event, context):
    # Generuj unikatowe ID
    unique_id = str(uuid.uuid4())
    
    # Odczytaj 'name' i 'filename' z ciała zapytania POST
    body = json.loads(event.get('body', '{}'))
    name = body.get('name', '')
    filename = body.get('filename', 'default-file-name')  # Używam 'default-file-name' jako nazwy domyślnej
    description = body.get('description','')
    # Połączenie z bazą danych
    conn = psycopg2.connect(
            user="postgres",
            password="123Askone",
            host="pbs-reader-2.cvn9enzqebxx.eu-north-1.rds.amazonaws.com",
            port="5432",
            database="pbs_reader"
    )

    # Inicjalizuj wynik i presigned_url
    result = {}
    presigned_url = ""
    
    try:
        # Wykonaj zapytanie SQL
        with conn.cursor() as cur:
            cur.execute("INSERT INTO productmanager.product (id, name, description, cover) VALUES (%s, %s, %s, %s);", (unique_id, name, description, filename))
        conn.commit()
        
        # Generuj presigned URL
        s3_client = boto3.client('s3')
        presigned_url = s3_client.generate_presigned_url('put_object',
                                                        Params={'Bucket': 'chapters-pbs',
                                                                'Key': f"{unique_id}/{filename}"},
                                                        ExpiresIn=3600)
        
        result = {'status': 'success', 'presigned_url': presigned_url}
        
    except Exception as e:
        result = {'status': 'error', 'error': str(e)}
    finally:
        # Zamknij połączenie
        conn.close()
        
    return {
        'statusCode': 200,
        'body': json.dumps(result),
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        }
    }