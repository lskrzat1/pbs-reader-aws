import json
import boto3
import psycopg2

s3 = boto3.client('s3')

def lambda_handler(event, context):
    try:
        # Odczytanie JSONa z ciała żądania
        body = json.loads(event["body"])
        
        # Pobranie parametrów z JSONa
        chapter_id = body.get("chapterId")
        product_id = body.get("productId")
        file_name = body.get("fileName")
        
        if not all([chapter_id, product_id, file_name]):
            return {
                'statusCode': 400,
                'body': json.dumps({'error': 'Missing parameters'}),
                'headers': {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Credentials': True,
                }
            }

        # Zapisanie do bazy danych PostgreSQL
        connection = psycopg2.connect(
            user="postgres",
            password="123Askone",
            host="pbs-reader-2.cvn9enzqebxx.eu-north-1.rds.amazonaws.com",
            port="5432",
            database="pbs_reader"
        )

        cursor = connection.cursor()
        cursor.execute("INSERT INTO productmanager.image_data (file_name, uploaded, product_id, chapter_id) VALUES (%s, %s, %s, %s)", (file_name, True, product_id, chapter_id))
        connection.commit()
        cursor.close()
        connection.close()

        # Tworzenie pre-signed URL
        bucket_name = "chapters-pbs"  # Zmień na nazwę swojego bucketu
        object_name = f"{product_id}/{chapter_id}/{file_name}"
        
        response = s3.generate_presigned_url('put_object',
                                             Params={'Bucket': bucket_name,
                                                     'Key': object_name,
                                             },
                                             ExpiresIn=36000)
        
        return {
            'statusCode': 200,
            'body': json.dumps({'url': response}),
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': True,
            }
        }
        
    except Exception as e:
        return {
            'statusCode': 500,
            'body': json.dumps({'error': str(e)}),
            'headers': {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': True,
            }
        }