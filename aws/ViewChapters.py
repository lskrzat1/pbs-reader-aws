import json
import psycopg2
from psycopg2.extras import RealDictCursor
import boto3

def generate_presigned_url(bucket_name, object_name):
    s3_client = boto3.client('s3')
    response = s3_client.generate_presigned_url('get_object',
                                                Params={'Bucket': bucket_name,
                                                        'Key': object_name},
                                                ExpiresIn=3600)  # Czas ważności linku w sekundach
    return response

def lambda_handler(event, context):
    try:
        chapter_id = event['queryStringParameters']['chapterId']
    except KeyError:
        return {
            'statusCode': 400,
            'body': json.dumps({'error': 'chapterId is required'})
        }

    try:
        conn = psycopg2.connect(
            user="postgres",
            password="123Askone",
            host="pbs-reader-2.cvn9enzqebxx.eu-north-1.rds.amazonaws.com",
            port="5432",
            database="pbs_reader"
        )
    except Exception as e:
        return {
            'statusCode': 500,
            'body': json.dumps({'error': 'Could not connect to database', 'details': str(e)})
        }

    results = []
    try:
        cursor = conn.cursor(cursor_factory=RealDictCursor)
        cursor.execute('SELECT * FROM productmanager.image_data WHERE chapter_id = %s', (chapter_id,))
        results = cursor.fetchall()
        
        bucket_name = "chapters-pbs"  # Nazwa twojego bucketu na S3
        for result in results:
            object_name = f"{result['product_id']}/{result['chapter_id']}/{result['file_name']}"
            result['s3_presigned_url'] = generate_presigned_url(bucket_name, object_name)

    except Exception as e:
        return {
            'statusCode': 500,
            'body': json.dumps({'error': 'Database query failed', 'details': str(e)})
        }
    finally:
        cursor.close()
        conn.close()

    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        },
        'body': json.dumps({'data': results})
    }