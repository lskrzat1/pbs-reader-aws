
import json
import boto3
import psycopg2

# Inicjalizacja klienta S3
s3_client = boto3.client('s3')

def lambda_handler(event, context):
    try:
        # Połączenie z bazą danych
        conn = psycopg2.connect(
               user="postgres",
                password="123Askone",
                host="pbs-reader-2.cvn9enzqebxx.eu-north-1.rds.amazonaws.com",
                port="5432",
                database="pbs_reader"
        )
        cursor = conn.cursor()
        
        # Odczytanie nagłówka Content-Type i ciała żądania
        content_type = event["headers"].get("Content-Type", "")
        body = bytes(event["body"], 'utf-8') if isinstance(event["body"], str) else event["body"]
        
        # Wyszukanie granicy (boundary) i podział ciała żądania na części
        try:
          boundary = content_type.split("boundary=")[1]
        except IndexError:
          return {
            'statusCode': 500,
            'body': json.dumps({'error': 'Boundary not found in Content-Type','Content-Type': content_type}),
            'headers': {'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': True,
        }
            }
        parts = body.split(bytes("--" + boundary, 'utf-8'))
        
        product_id, chapter_id, file_data, file_name = None, None, None, None

        # Wyszukanie części z productId, chapterId i plikiem
        for part in parts:
            if b'name="productId"' in part:
                product_id = part.split(b'\r\n\r\n')[1].strip().decode()
            elif b'name="chapterId"' in part:
                chapter_id = part.split(b'\r\n\r\n')[1].strip().decode()
            elif b'name="file"' in part:
                headers, file_data = part.split(b'\r\n\r\n', 1)
                file_data = file_data.rstrip(b'\r\n--')
                file_name = headers.decode().split('filename="')[1].split('"')[0]
        
        if product_id and chapter_id and file_data:
            # Tworzenie folderu w S3 i zapisywanie pliku
            bucket_name = "chapters-pbs"  # Zmień na nazwę swojego bucketu
            folder_name = f"{product_id}/{chapter_id}/"
            s3_client.put_object(Bucket=bucket_name, Key=folder_name)
            s3_client.put_object(Body=file_data, Bucket=bucket_name, Key=f"{folder_name}{file_name}")
    
          # Zapis danych do bazy
            cursor.execute("""
                INSERT INTO productmanager.image_data (File_name, Uploaded, Product_id, Chapter_id) 
               VALUES (%s, %s, %s, %s)
            """, (file_name, True, product_id, chapter_id))
            conn.commit()

            cursor.close()
            conn.close()

            return {
                'statusCode': 200,
                'body': json.dumps({'productId': product_id, 'chapterId': chapter_id, 'file_name': file_name, 'message': 'Folder i plik utworzone', 'Content-Type': content_type}),
                'headers': {'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': True,
                  }
            }

        cursor.close()
        conn.close()

        return {
            'statusCode': 400,
            'body': json.dumps({'error': 'Nie znaleziono productId, chapterId lub pliku', 'Content-Type': content_type, 'Product_id': product_id}),
            'headers': {'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                        'Access-Control-Allow-Credentials': True,
                  }
        }
        
    except Exception as e:
        return {
            'body': json.dumps({
                'error': str(e),
                'Content-Type': content_type  # Dodajemy tutaj
            }),
            'body': json.dumps({'error': str(e)}),
                'headers': {'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Credentials': True,
                  }
        }