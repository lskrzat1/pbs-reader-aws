import json
import psycopg2
import os
import boto3

def generate_presigned_url(bucket_name, object_name):
    s3_client = boto3.client('s3')
    try:
        response = s3_client.generate_presigned_url('get_object',
                                                    Params={'Bucket': bucket_name,
                                                            'Key': object_name},
                                                    ExpiresIn=3600)
    except Exception as e:
        return None
    return response

def lambda_handler(event, context):
    try:
        conn = psycopg2.connect(
            user="postgres",
            password="123Askone",
            host="pbs-reader-2.cvn9enzqebxx.eu-north-1.rds.amazonaws.com",
            port="5432",
            database="pbs_reader"
        )

        cursor = conn.cursor()
        cursor.execute("SELECT id, name, description, cover FROM productmanager.product;")
        rows = cursor.fetchall()

        cursor.close()
        conn.close()

        bucket_name = 'chapters-pbs'

        result = []
        for row in rows:
            id, name, description, cover = row
            object_name = f"{id}/{cover}"  # Użycie zmiennej cover z bazy danych
            presigned_url = generate_presigned_url(bucket_name, object_name)

            result.append({
                "id": id,
                "name": name,
                "description": description,
                "cover": cover,
                "presigned_url": presigned_url
            })

        return {
            'statusCode': 200,
            'headers': {
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': json.dumps(result)
        }
    except Exception as e:
        return {
            'statusCode': 500,
            'headers': {
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
            },
            'body': str(e)
        }
